#!/bin/bash

SRCDIR="$(dirname "$0")"

absolute() {
    case "$1" in
	/*)
	    echo "$1"
	    ;;
	*)
	    echo "$(pwd)/$t"
	    ;;
    esac
}

SRCDIR="$(absolute "$SRCDIR")"
readonly SRCDIR

# Set env for the scripts and test scripts:
readonly DATADIR="$SRCDIR"
if [[ "$PATH" ]]; then
    readonly PATH="$SRCDIR:$PATH"
else
    readonly PATH="$SRCDIR"
fi
export DATADIR PATH

if [[ "$1" ]]; then
    readonly TESTS=("$@")
else
    readonly TESTS=("$SRCDIR"/tests/*.sh)
fi

status=0
for t in "${TESTS[@]}"; do
    echo -n "################################### "
    echo "Running $(basename "$t")..."

    # The absolute path will be needed after a pushd:
    t="$(absolute "$t")"

    # Clean up what has been left by previous tests:
    rm -rf "$TMP"/test-gitgitconfig

    mkdir "$TMP"/test-gitgitconfig
    pushd "$TMP"/test-gitgitconfig
    if "$t"; then echo OK; else echo FAILED; let '++ status'; fi
    popd
done

echo "$status tests failed."

# Clean up if there have been no errors. Otherwise leave it for inspection.
if (( status == 0 )); then
    rm -rf "$TMP"/test-gitgitconfig
fi

exit $status
