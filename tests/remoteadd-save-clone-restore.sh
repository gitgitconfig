#!/bin/bash -ex

mkdir A
cd A

git init
echo a > a
git add a
git commit -m 'A.'
git remote add there git://example.com/a.git
gitgitconfig-save

cd ..

git clone -o first A B
cd B

gitgitconfig-restore first

if [[ "$(git remote)" == "$(echo there)" ]]; then
    exit 0
else
    exit 1
fi
