#!/bin/bash -ex

git init --bare server.git

mkdir A
cd A

git init
echo a > a
git add a
git commit -m 'A.'
git remote add server "$(cd ..; pwd)"/server.git
git push -u --mirror server
gitgitconfig-save
git push -u --mirror server

cd ..

git clone -o server server.git B
cd B

gitgitconfig-restore server

if [[ "$(git remote)" == "$(echo server)" ]]; then
    exit 0
else
    exit 1
fi
