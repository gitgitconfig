#!/bin/bash -ex

mkdir A
cd A

git init
echo a > a
git add a
git commit -m 'A.'
git remote add there git://example.com/a.git
gitgitconfig-init
pushd .git
git add config
git commit -m '"there" remote added.'
popd
gitgitconfig-save

cd ..

git clone -o first A B
cd B

gitgitconfig-init
# Unfortunately, we have to add the inital state of the config manually.
pushd .git
git add config
git commit -m 'Initial conf after clone.'
popd
gitgitconfig-restore first

if [[ "$(git remote)" == "$(echo there)" ]]; then
    exit 0
else
    exit 1
fi
